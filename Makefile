CC=g++
CPPFLAGS=-std=c++98 -pedantic -Wextra -g

all: ouisearch

ouisearch: ouisearch.cpp ouisearch.hpp
	$(CC) ouisearch.cpp $(CPPFLAGS) -o ouisearch

pack:
	tar -cf xhrade08.tar Makefile ouisearch.cpp ouisearch.hpp Readme manual.pdf
