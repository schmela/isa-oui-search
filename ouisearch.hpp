#ifndef OUISEARCH_H
#define OUISEARCH_H

/*
======================================================
Projekt ISA(S�ov� aplikace a spr�va s�t�) 2012/2013:
Mapov�n� adresov�ho prostoru IPv6 pomoc� OUI
======================================================

Autor: Michal Hradeck� - xhrade08@stud.fit.vutbr.cz
------------------------------------------------------
*/

#include <iostream>
#include <string>

using namespace std;

typedef struct TParams
{
	string prefix;
	int prefix_len;
	string filename;
}TParams;

class badParameters{};
class badIPv6{};
class openFileError{};
class mallocError{};
class socketError{};
class sendtoError{};
class recvfromError{};
class recvmsgError{};
class closeError{};
class forkError{};
class emptyOUIFileError{};

string regexp(string source, string pattern);
class regexpCompilationError{};

void kill_child(void);
int StrToInt (string in);
void create_address_and_send (int prefix_len, string prefix, int sock_send, unsigned char * packet, struct sockaddr_in6 sa, std::map<std::string,std::string> *ouis);
TParams *parseParams(int argc,char *argv[]);

#endif
