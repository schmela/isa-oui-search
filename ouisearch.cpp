/*
======================================================
Projekt ISA(S�ov� aplikace a spr�va s�t�) 2012/2013:
Mapov�n� adresov�ho prostoru IPv6 pomoc� OUI
======================================================

Autor: Michal Hradeck� - xhrade08@stud.fit.vutbr.cz
------------------------------------------------------
*/

#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <fstream>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/icmp6.h>
#include <netinet/ip.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "ouisearch.hpp"

using namespace std;

// pid potomka, ktery pouze prijima ICMPv6 zpravy
pid_t pid_potomek;

#define ICMP_HDRLEN 8
#define OUI_LENGTH 8

// zabije potomka
void kill_child(void)
{
    int status;
    kill(pid_potomek,SIGTERM);
    waitpid (pid_potomek,&status,0);
}

// prevod stringu na integer
int StrToInt (string in)
{
    istringstream s(in);
    int out;
    s >> out;
    return out;
}

// zpracovani parametru
TParams *parseParams(int argc,char *argv[])
{
    if (argc==2)
    {
        if (strcmp (argv[1],"-h") == 0)
        {
            cout <<	"OUISEARCH - vyhledavani modifikovanych EUI-64 v zadane siti"<<endl<<
                 "pouziti:"<<endl<<
                 "ouisearch -h vypise tuto napovedu"<<endl<<
                 "ouisearch -p sit/rozsah -d filename"<<endl<<
                 "-p specifikuje prvni cast adresy (a jeji rozsah), ktera bude hledana"<<endl<<
                 "-d soubor, ve kterem je ulozena databaze OUI"<<endl<<flush;
            exit(EXIT_SUCCESS);
        }
        else
        {
            throw badParameters();
        }
    }
    else
    {

        TParams * params = new (TParams);
        int p=0,d=0; // pocty kolikrat ktery argumemt je
        if (argc!=5) throw badParameters();
        for (int i=1; i<argc-1; i++)
        {
            if ((strcmp(argv[i],"-p")==0)&&(p++==0))
            {
                params->prefix = regexp(argv[i+1],"^([1234567890abcdefABCDEF:]+)/[[:digit:]]+");
                params->prefix_len = StrToInt(regexp(argv[i+1],"^[1234567890abcdefABCDEF:]+/([[:digit:]]+)"));
                if (strcmp(params->prefix.c_str(),"")==0)
                {
                    delete (params);
                    throw badIPv6();
                }
                //cout << "IPv6: " << params->prefix << endl;
                //cout << "range: " << params->prefix_len << endl;

                if (params->prefix_len > 64)
                {
                    delete (params);
                    throw badIPv6();
                }

                i++;
            }
            else if ((strcmp(argv[i],"-d")==0)&&(d++==0))
            {
                params->filename = argv[i+1];
                i++;
            }
            else
            {
                delete (params);
                throw badParameters();
            }

        }

        return params;
    }
}

// vrati obsah prvni zavorky z regexpu, kdyz neni nalezena, vraci se ""
string regexp(string source, string pattern)
{
    regex_t regex;
    int c;

    // kompilace regulerniho vyrazu
    try
    {
        c = regcomp(&regex, pattern.c_str(), REG_EXTENDED);
        if ( c != 0 )
        {
            throw regexpCompilationError();
        }
    }
    catch (regexpCompilationError)
    {
        cerr << "Regex compilation Error" << endl;
        regfree(&regex);
        exit ( EXIT_FAILURE );
    }

    // vykonani regulerniho vyrazu
    size_t     nmatch = 2;
    regmatch_t pmatch[2];
    if ((regexec(&regex,source.c_str(),nmatch,pmatch,0)) == 0)
    {
        int begin = (int)pmatch[1].rm_so;
        int end = (int)pmatch[1].rm_eo;
        int length = end-begin;
        regfree(&regex);
        if ((begin==-1)||(end==-1)) return "";
        else return source.substr(begin,length);
    }
    else
    {
        // kdyz se nic nenaslo, vraci se prazdny retezec
        regfree(&regex);
        return "";
    }
}


void create_address_and_send (int prefix_len, string prefix, int sock_send ,unsigned char * packet, struct sockaddr_in6 sa, std::map<std::string,std::string> *ouis)
{
    string hexchars = "0123456789ABCDEF";
    //string hexchars = "AB";

    if (prefix_len == 64)
    {
        //cout << "64>>> " << prefix << "/" << prefix_len << endl;
        // mame prvnich 64 bitu, ted tech dalsich 64

        // ted budeme zkouset vsechny nalezene OUI
        map<string,string>::iterator it;
        for (it = ouis->begin(); it != ouis->end(); ++it)
        {
            string oui = it->first;
            //oui[1]=oui.c_str()[1] ^ 0b00000010; // inverze sedmeho bitu


            string buf = prefix;
            // k prefixu pridame vybrane OUI z databaze + retezec "FFFE" podle RFC
            if (prefix.at(prefix.length()-1) != ':') buf.push_back(':');
            buf.append(oui,0,2);
            buf.append(oui,3,2);
            buf.push_back(':');
            buf.append(oui,6,2);
            buf.append("FF:FE");


            create_address_and_send(prefix_len+40, buf ,sock_send , packet, sa, ouis);

        }

    }
    else if (prefix_len == 128)
    {
        // mame vsech 128 bitu adresy, posilame

        //cout << "POSILAME na: " << prefix << "/" << prefix_len << endl;

        // prevod adresy ve stringu do sitove podoby
        inet_pton(AF_INET6,prefix.c_str(),&(sa.sin6_addr));

        // prevraceni 7. bitu OUI
        char *pom = (char*)&sa.sin6_addr; // abych si mohl adresu rozkouskovat po bytech
        pom[8] ^= 0x2; // 8. byte z celkove IPv6 adresy XORuji s hodnotou 00000010 (inverze 7. bitu OUI)

        // samotne poslani paketu na 1 adresu
        if ( (sendto(sock_send, (void *) packet, 8, 0, (struct sockaddr *)&sa, sizeof(sa))  ) < 0)
        {
            throw sendtoError();
        }

        // pockame 100 mikrosekund aby nepretekly buffery
        usleep(100);

    }
    else
    {
        // adresa jeste neni dost dlouha, pridame dalsi znak

        // po kazde cvterici znaku (16 bitech) musi nasledovat :
        if (prefix_len % 16 == 0) prefix.push_back(':');

        // pridame znak (+4b) a zanorime rekurzi hloubeji
        for (unsigned int i=0; i < hexchars.length(); ++i)
        {
            string buf = prefix;
            create_address_and_send(prefix_len+4, buf+=(hexchars[i]) ,sock_send , packet, sa, ouis);
        }
    }

}

int main(int argc,char *argv[])
{
    try
    {
        // zpracovani parametru
        TParams * parametry = parseParams(argc,argv);

        // nacteni souboru s OUI do pameti
        // mapa pro ukladani OUI <24b IPv6 adresa,jmeno spolecnosti>
        std::map<std::string,std::string> ouis;

        // otevreni do souboru
        ifstream file_ouis;
        file_ouis.open(parametry->filename.c_str(), ios_base::in);
        if (! file_ouis.is_open())
        {
            // otevreni souboru se nepovedlo
            throw openFileError();
        }
        else
        {
            // prochazeni pres vsechny radky souboru
            string line, oui, organization;
            while ( file_ouis.good() )
            {
                // jeden radek souboru
                getline (file_ouis,line);

                // vytazeni oui z radku
                oui = regexp(line,"^([1234567890abcdefABCDEF:]+)[.]*");

                // vytazeni nazvu organizace z radku
                organization = "";
                if ((oui.length()+1) < line.length() )
                    organization = line.substr(oui.length()+1);

                // kontrola spravnosti z jednoho radku
                if (oui.length()==OUI_LENGTH)
                {
                    //cout << "ok oui "<< line << endl;
                    ouis.insert(std::make_pair(oui,organization));
                }
                else
                {
                    //cout << "bad oui "<< line << endl;
                }
            }
        }

        // zavreni souboru s OUI
        file_ouis.close();

        // pokud nebyla v souboru zadana OUI, tak se muze rovnou skoncit
        if (ouis.empty())
        {
            throw emptyOUIFileError();
        }

        // IPv6 adresa
        struct sockaddr_in6 sa;
        sa.sin6_family = AF_INET6;
        sa.sin6_port = htons(0);

        // ICMPv6 hlavicka
        struct icmp6_hdr icmphdr;

        // typ icmp zpravy (1B)
        icmphdr.icmp6_type = ICMP6_ECHO_REQUEST;

        // kod zpravy (1B)
        icmphdr.icmp6_code = 0;

        // identifikator (2B)
        icmphdr.icmp6_id = htons (1000);

        // cislo sekvence - zacina v 0 (2B)
        icmphdr.icmp6_seq = htons (0);


        // fork
        // -----
        pid_potomek=fork();
        if ( pid_potomek==-1 )
        {
            // fork se nepovedl -> zavrit vsechny potomky
            throw forkError();
        }
        else if ( pid_potomek==0 )
        {

            // jsme v potomkovi
            // prijimaji se tu prichozi ICMPv6 zpravy

            // alokace mista pro prichozi paket
            unsigned char * packet2;
            if ((packet2 = (unsigned char *) malloc (sizeof(icmphdr)*2) ) == NULL)
            {
                throw mallocError();
            }

            struct sockaddr_storage addr;
            char ipstr[INET6_ADDRSTRLEN];
            char ipstr2[INET6_ADDRSTRLEN];
            socklen_t fromlen = sizeof(addr);

            // stvoreni socketu pro prijmuti ICMPv6 paketu
            int sock_recv;
            if ((sock_recv = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6)) == -1)
            {
                throw socketError();
            }

            int bits_recieved=1;
            while (bits_recieved)
            {
                // prijmuti ICMPv6 paketu
                if ( (bits_recieved = recvfrom(sock_recv, (void *) packet2,sizeof(icmphdr)*2 , 0, (struct sockaddr *)&addr, &fromlen) ) < 0)
                {
                    throw recvfromError();
                }

                inet_ntop(AF_INET6,&(((struct sockaddr_in6 *)(&addr))->sin6_addr), ipstr, sizeof(ipstr));
                //inet_ntop(AF_INET6,&iphdr.ip6_dst, ipstr2, sizeof(ipstr));

                //  vytahnuti OUI z prijate adresy
                string hexchars = "0123456789ABCDEF";
                string recv_oui="";

                // pom[8] ukazuje na prvni byte z OUI v prijate adrese, pom[9] na druhy...
                char *pom;
                pom = (char *) &(((struct sockaddr_in6 *)(&addr))->sin6_addr);

                // vyzvedneme si vzdy 1B OUI, pulku jej vymaskujeme (& 0x0F) (popr shiftneme o 4b doprava ( >> 4))
                // to nam index kde se nachazi znak s hexa cislici ve stringu hexchars
                recv_oui.push_back(hexchars.at( (pom[8] >> 4) & 0x0F ));
                recv_oui.push_back(hexchars.at( (pom[8] & 0x0F) ^ 0x02 )); // navic inverze sedmeho bitu pomoci XOR 0b00000010
                recv_oui.push_back(':');
                recv_oui.push_back(hexchars.at( (pom[9] >> 4) & 0x0F ));
                recv_oui.push_back(hexchars.at( pom[9] & 0x0F ));
                recv_oui.push_back(':');
                recv_oui.push_back(hexchars.at( (pom[10] >> 4) & 0x0F ));
                recv_oui.push_back(hexchars.at( pom[10] & 0x0F ));
                //cout << "prijate OUI:" << recv_oui << endl;


                // typ ICMPv6
                int type = (int) (((icmp6_hdr *)(packet2))->icmp6_type);
                if (type == ICMP6_ECHO_REPLY)
                {
                    //cout << "icmpv6-typ:" << "ICMP6_ECHO_REPLY" << endl;

                    // tisk destination prijimane adresy + popr nazev spolecnosti jiz patri dane OUI
                    string oui = "";
                    oui = ouis.find(recv_oui)->second;
                    if (oui.length() > 0)
                        cout << ipstr << " " << oui << endl;
                    else
                        cout << ipstr << endl;

                }
                else
                {
                    // neni to ECHO_REPLY, nic se nevypisuje
                    //cout << "icmpv6-typ:" << type << endl;
                }

            }

            // zavreni socketu
            if ( ( close(sock_recv)) < 0)
            {
                throw closeError();
            }

            // uvolneni pameti paketu
            free(packet2);

            delete (parametry);

        }
        else
        {
            // jsme v rodici
            // posilaji se tu vsechny ICMPv6 pozadavky

            // alokace mista pro data ICMPv6 paketu
            unsigned char * packet = NULL;
            if ((packet = (unsigned char *) malloc (sizeof(icmphdr)) ) == NULL)
            {
                throw mallocError();
            }

            // kopie ze struktury do stringu
            memcpy (packet, &icmphdr, ICMP_HDRLEN);

            // stvorime raw socket s protokolem ICMPv6
            int sock_send;
            if ((sock_send = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6)) == -1)
            {
                throw socketError();
            }

            //posleme pakety na vsechny adresy z rozsahu - rekurzivni funkce co tvori adresy a az ma vsech 128b tak na ni posle ICMPv6
            //cout << "Prefix: " << parametry->prefix << "/" << parametry->prefix_len << endl;
            create_address_and_send(parametry->prefix_len, parametry->prefix, sock_send, packet, sa, &ouis);

            // pockame 3 sekundy
            usleep(3000000);

            // uvolnime packet
            free(packet);

            // a zabijeme potomka
            kill_child();

            // zavreni socketu
            if ( (close(sock_send)) < 0)
            {
                throw closeError();
            }

        }


        delete (parametry);
    }

    catch (badParameters)
    {
        cerr << "Spatne parametry" << endl;
        return EXIT_FAILURE;
    }

    catch (badIPv6)
    {
        cerr << "Spatne zadana IPv6 adresa." << endl;
        return EXIT_FAILURE;
    }

    catch (openFileError)
    {
        perror("open");
        cerr << "Zadany soubor nelze otevrit." << endl;
        return EXIT_FAILURE;
    }

    catch (socketError)
    {
        perror("socket");
        cerr << "socket() error: Nepovedlo se vytvorit socket." << endl;
        return EXIT_FAILURE;
    }

    catch (sendtoError)
    {
        perror("sendto");
        cerr << "sendto() error: Nepodarilo se zaslat zpravu." << endl;
        kill_child();
        return EXIT_FAILURE;
    }

    catch (recvfromError)
    {
        perror("recvfrom");
        cerr << "recvfrom() error: Nepodarilo se prijmout odpoved." << endl;
        return EXIT_FAILURE;
    }

    catch (recvmsgError)
    {
        perror("recvmsg");
        cerr << "recvmsg() error: Nepodarilo se prijmout zpravu." << endl;
        return EXIT_FAILURE;
    }

    catch (closeError)
    {
        perror("socket");
        cerr << "close() error: Nepovedlo se zavrit socket." << endl;
        return EXIT_FAILURE;
    }

    catch (forkError)
    {
        perror("fork");
        cerr << "fork() error: Nepovedlo se rozdvojit proces." << endl;
        kill_child();
        return EXIT_FAILURE;
    }

    catch (mallocError)
    {
        perror("malloc");
        cerr << "malloc() error: Nepovedlo se alokovat pamet." << endl;
        return EXIT_FAILURE;
    }

    catch (emptyOUIFileError)
    {
        cerr << "Zadany soubor neobsahuje zadne platne OUI." << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
